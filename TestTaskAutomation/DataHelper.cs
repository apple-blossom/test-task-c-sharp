﻿

using System;

namespace TestTaskAmeria
{
    class DataHelper
    {
        public static string URL = "";
        public static string UserEmail = "";
        public static string UserPass = "";
        public static string FormTitle = "TestTask" + DateTime.Now.ToString(); 
        public static string TimeZone = "Kiev";
        public static string Width = "800";
        public static string Height = "800";

    }
}
