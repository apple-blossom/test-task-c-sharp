﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using OpenQA.Selenium.Chrome;

namespace TestTaskAmeria
{
    public class BasePage
    {

        private static IWebDriver webDriver;

        public static IWebDriver WebDriver
        {
            get
            {
                if (webDriver == null)
                {
                    ChromeOptions options = new ChromeOptions();
                    options.AddArgument("--start-maximized");
                    webDriver = new ChromeDriver(".", options);
                }

                return webDriver;
            }

        }

        public By LoaderLocator = By.CssSelector(".vb-loader");
        public WebDriverWait Wait(int time = 20) => new WebDriverWait(WebDriver, TimeSpan.FromSeconds(time));


        public void WaitForElementToLoad(By waitFor, int time = 20)
        {
            Wait(time).Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(waitFor));
        }

        public void WaitForMultipleElementToLoad(By waitFor, int time = 20)
        {
            Wait(time).Until(SeleniumExtras.WaitHelpers.ExpectedConditions.VisibilityOfAllElementsLocatedBy(waitFor));
        }

        public void WaitForElementToDisappear(By waitFor, int time = 20)
        {
            Wait(time).Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(waitFor));
        }

        public void WaitLoaderToDisappear(int time = 20)
        {
            WaitForElementToDisappear(LoaderLocator, time);
        }

        public void ClickButtonAndWait(By buttonLocator, By waitFor)
        {
            IWebElement button = WebDriver.FindElement(buttonLocator);
            button.Click();
            WaitForElementToLoad(waitFor);
        }

        public void OpenUrl(string url)
        {
            WebDriver.Navigate().GoToUrl(url);
        }

        public void TearDownTest()
        {
            WebDriver.Quit();
        }

    }
}
