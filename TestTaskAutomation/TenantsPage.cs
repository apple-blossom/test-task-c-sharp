﻿using NUnit.Framework;
using OpenQA.Selenium;

namespace TestTaskAmeria
{
    class TenantsPage : BasePage
    {
        public By TenantLocator = By.XPath("//span[contains(text(),'AutoTest')]");
        public By PlacesHeaderLocator = By.XPath("//span[contains(text(),'Places')]");
        public By AddNewBtnLocator = By.XPath("//span[contains(text(),'Add new')]");
        public By BackBtnLocator = By.CssSelector(".vb-btn-back");
        public By ItemsLocator = By.CssSelector(".vb-pict-preview");

        public By GetElementWithText(string text) => By.XPath($"//span[contains(text(),'{text}')]");

        public void ClickTenantAndWait()
        {
            WebDriver.FindElement(TenantLocator).Click();
            WaitLoaderToDisappear();
            WaitForElementToLoad(AddNewBtnLocator);
        }

        public void ClickAddNewAndWait()
        {
            NewPlacesPage np = new NewPlacesPage();
            WebDriver.FindElement(AddNewBtnLocator).Click();
            WaitLoaderToDisappear();
            WaitForElementToLoad(np.TitleLocator);
        }

        public void ClickBackToTenantListAndWait()
        {
            WebDriver.FindElement(BackBtnLocator).Click();
            WaitLoaderToDisappear();
            WaitForMultipleElementToLoad(ItemsLocator);
        }

        public void VerifyElementCreated(string elementText)
        {
            var elements = WebDriver.FindElements(GetElementWithText(elementText));
            Assert.IsNotEmpty(elements);
        }

    }
}
