﻿using System.Collections.Generic;
using NUnit.Framework;
using OpenQA.Selenium;

namespace TestTaskAmeria
{
    class NewPlacesPage : BasePage
    {
        public By TitleLocator = By.XPath("//vb-input[contains(.,'Title')]//input[contains(@class, 'vb-form-input')]");
        public By TitleResultLocator = By.XPath("//vb-input[contains(.,'Title')]//span[contains(@class, 'multiline')]");
        public By SelectLocator = By.XPath("//vb-dropdown[contains(.,'Timezone')]//vb-select//span[contains(@class, 'select2-selection--single')]");
        public By SelectFromMenuLocator = By.XPath("//input[contains(@class, 'select2-search__field')]");
        public By DeviceTypeLocator = By.XPath("//vb-dropdown[contains(.,'Device Type')]//vb-select");
        public By WinWorkstationLocator = By.XPath("//div[contains(@title,'Windows Workstation')]");
        public By ConfigurationLocator = By.XPath("//div[contains(text(),'Configuration')]");
        public By ScreenLocator = By.XPath("//span[contains(text(),'Screen')]");
        public By ScreenWidthLocator = By.XPath("//vb-numeric-input[contains(.,'Screen Width')]//input[contains(@class, 'vb-form-input')]");
        public By ScreenWidthResultLocator = By.XPath("//vb-numeric-input[contains(.,'Screen Width')]//*[contains(@class, 'multiline')]");
        public By ScreenHeightLocator = By.XPath("//vb-numeric-input[contains(.,'Screen Height')]//input[contains(@class, 'vb-form-input')]");
        public By ScreenHeightResultLocator = By.XPath("//vb-numeric-input[contains(.,'Screen Height')]//*[contains(@class, 'multiline')]");
        public By ErrorLocator = By.CssSelector(".vb-validation-msg");
        public By GlobSpinnerLocator = By.CssSelector(".vb-glob-loader");
        public By SubmitLocator = By.XPath("//span[contains(text(),'Submit')]");
        public By DeleteLocator = By.XPath("//span[contains(text(),'Delete')]");


        public void FillInNewLocationForm(string title, string timeZone)
        {
            WebDriver.FindElement(TitleLocator).SendKeys(title);
            ClickButtonAndWait(SelectLocator, SelectFromMenuLocator);
            WebDriver.FindElement(SelectFromMenuLocator).SendKeys(timeZone);
            string locator = $"//div[contains(text(),'{timeZone}')]";
            IReadOnlyCollection<IWebElement> result =  WebDriver.FindElements(By.XPath(locator));
            if (result.Count > 0)
            {
                WebDriver.FindElement(By.XPath(locator)).Click();
            }
        }

        public void FillInDeviceTypeWindows()
        {
            WebDriver.FindElement(DeviceTypeLocator).Click();
            WaitForElementToLoad(WinWorkstationLocator);
            WebDriver.FindElement(WinWorkstationLocator).Click();
            WaitLoaderToDisappear();
        }

        public void WaitForConfigFormToShowAndOpenScreenSettings()
        {
            WaitForElementToLoad(ScreenLocator);
            ClickButtonAndWait(ScreenLocator, ScreenWidthLocator);
        }

        public void FillInScreenSizeForm(string width, string height)
        {
            EditScreenSizeField(width, ScreenWidthLocator);
            EditScreenSizeField(height, ScreenHeightLocator);
        }

        public void CheckScreenWidthValidation(string invalidWidth)
        {
            WebDriver.FindElement(ScreenWidthLocator).Click();
            EditScreenSizeField(invalidWidth, ScreenWidthLocator);
            var err = WebDriver.FindElements(ScreenLocator);
            Assert.IsNotEmpty(err);
        }

        public void EditScreenSizeField(string value, By element)
        {
            var field = WebDriver.FindElement(element);
            field.Click();
            field.Clear();
            field.SendKeys(value);
        }

        public void SubmitForm()
        {
            WebDriver.FindElement(SubmitLocator).Click();
            WaitForElementToDisappear(GlobSpinnerLocator);
            WaitLoaderToDisappear();
        }

        public void VerifyDataSavedCorrectly(string title, string width, string height)
        {
            WaitForElementToLoad(ScreenLocator);
            string nTitle = WebDriver.FindElement(TitleResultLocator).Text;
            WebDriver.FindElement(ScreenLocator).Click();
            WaitLoaderToDisappear();
            WaitForElementToLoad(ScreenHeightResultLocator);
            string nWidth = WebDriver.FindElement(ScreenWidthResultLocator).Text;
            string nHeight = WebDriver.FindElement(ScreenHeightResultLocator).Text;
            Assert.AreEqual(title, nTitle);
            Assert.AreEqual(width + " px", nWidth);
            Assert.AreEqual(height + " px", nHeight);
        }

    }
}
