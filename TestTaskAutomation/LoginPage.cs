﻿using OpenQA.Selenium;

namespace TestTaskAmeria
{
    class LoginPage : BasePage
    {
        TenantsPage tp = new TenantsPage();
        public By EmailFieldLocator = By.CssSelector("#email");
        public By PassFieldLocator = By.CssSelector("#password");
        public By LoginBtnLocator = By.XPath("//span[contains(text(),'Login')]");

        public void WaitForLoginPageToLoad()
        {
            WaitForElementToLoad(EmailFieldLocator);
        }


        public void FillInLoginform(string usrEmail, string usrPass)
        {
            WebDriver.FindElement(EmailFieldLocator).SendKeys(usrEmail);
            WebDriver.FindElement(PassFieldLocator).SendKeys(usrPass);
        }

        public void SubmitLogin()
        {
            WebDriver.FindElement(LoginBtnLocator).Click();
        }

        public void CheckIfLogined()
        {
            WaitLoaderToDisappear();
            WaitForElementToLoad(tp.TenantLocator);
        }

    }
}
