using System;
using NUnit.Framework;
using TestTaskAmeria;

namespace Tests
{
    public class Tests
    {
        LoginPage loginPage = new LoginPage();
        TenantsPage tenantsPage = new TenantsPage();
        NewPlacesPage newPlacesPage = new NewPlacesPage();
        BasePage basePage = new BasePage();
        public string title = DataHelper.FormTitle;

        [SetUp]
        public void Setup()
        {
            basePage.OpenUrl(DataHelper.URL);
        }

        [Test]
        public void TestScreenWidth()
        {
            loginPage.WaitForLoginPageToLoad();
            loginPage.FillInLoginform(DataHelper.UserEmail, DataHelper.UserPass);
            loginPage.SubmitLogin();
            loginPage.CheckIfLogined();
            tenantsPage.ClickTenantAndWait();
            tenantsPage.ClickAddNewAndWait();
            newPlacesPage.FillInNewLocationForm(title, DataHelper.TimeZone);
            newPlacesPage.FillInDeviceTypeWindows();
            newPlacesPage.WaitForConfigFormToShowAndOpenScreenSettings();
            newPlacesPage.CheckScreenWidthValidation("10");
            newPlacesPage.FillInScreenSizeForm("800", "800");
            newPlacesPage.SubmitForm();
            newPlacesPage.VerifyDataSavedCorrectly(title, DataHelper.Width, DataHelper.Height);
            tenantsPage.ClickBackToTenantListAndWait();
            tenantsPage.VerifyElementCreated(title);
        }

        [TearDown]
        public void TearDown()
        {
            basePage.TearDownTest();
        }

    }
}